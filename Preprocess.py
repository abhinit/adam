#!/usr/bin python2.7
"""
Author: Abhinit Ambastha
Email: abhinit@comp.nus.edu.sg

This function preprocesses the MRI scans. The 'pre-preprocessing' has been
carried out by Parvathy Sudhir Pillai. The new pre-preprocessing includes the 
following stages:

1. Extract masks from AAL template
2. Apply masks to extract the brain region from the brain nii file
3. Store the extracted ROI
"""
import sys, logging, time, os, csv
from os import path
import numpy as np
from numpy.linalg import inv
import nibabel
from nilearn import plotting, image, masking
from scipy import ndimage
import matplotlib.pyplot as plt
from clint.textui import colored
import cPickle as pi
from pyfiglet import Figlet


class Preprocessor(object):
	"""This class provides a pre-processor for incoming MRI data"""

	labels = None

	aalTemplateImage = None
	aalTemplateImageInputData = None
	aalTemplateImageInputHeader = None

	mniInputImage = None
	mniInputImageInputData = None
	mniInputImageInputHeader = None

	roiDimensions = np.array((33,57,49))

	def __init__(self,template,labels):
		#initialize the template
		self.aalTemplateImage = nibabel.load(template)
		self.aalTemplateImageInputData = self.aalTemplateImage.get_data()
		self.loadLabels(labels)

	def attachFile(self,fileName):
		#attach an MNI file for patch extraction
		self.mniInputImage = nibabel.load(fileName)
		#resampling for applying the mask
		self.mniInputImage = image.resample_img(self.mniInputImage, target_affine=np.array([[2,0,0,-90],[0,2,0,-126],[0,0,2,-72],[0,0,0,1]]), target_shape=self.aalTemplateImageInputData.shape, interpolation='continuous', copy=True, order='F')
		self.mniInputImageInputData = self.mniInputImage.get_data()
		self.mniInputImageInputHeader = self.mniInputImage.get_header()

	def loadLabels(self,labelFile):
		self.labels = dict(zip([line.split()[0] for line in open(labelFile).readlines()],[line.split()[1] for line in open(labelFile).readlines()]))

	def affineTransform(self,matrix,transform):
		X,Y,Z = matrix.shape
		tx,ty,tz = None,None,None
		resMatrix = np.empty(self.mniInputImageInputData.shape)
		for x in range(X):
			for y in range(Y):
				for z in range(Z):
					tx,ty,tz = np.dot(transform,[x,y,z])
					resMatrix[tx,ty,tz] = matrix[x,y,z]
		print tx,ty,tz
		return resMatrix

	def _createMask(self,label,name=None):
		if name is None:
			name = label
		X,Y,Z = self.aalTemplateImageInputData.shape
		extractedRegion = np.empty([X,Y,Z])
		for x in range(X):
			for y in range(Y):
				for z in range(Z):
					if self.aalTemplateImageInputData[x,y,z] == int(label):
						extractedRegion[x,y,z] = 1
					else:
						extractedRegion[x,y,z] = 0
		print "saving ROI mask "+name+" ...",
		print  colored.green('\t[DONE]')
		with open("mask/"+name+".npy", 'wb') as numpyFile:
			np.save(numpyFile,extractedRegion)

	def createMasks(self):
		done = 0
		startTime = time.time()
		print "\nBeginning mask extraction"
		print "\nExtracting mask: "

		for label,index in self.labels.items():
			self._createMask(index,name=label)
			done += 1

		print colored.blue('\n'+u'\u2713'+' '+ str(done)\
			+" masks extracted successfully\n")
		print "Time taken: "+str(format((time.time() - startTime),'.2f'))+" seconds"
		# Time taken: 651.84 seconds

	def _extractROI(self,maskRegion,label,outputDir):
		extractedRegion = np.multiply(self.mniInputImageInputData,maskRegion)

		extractedRegionNifTI = nibabel.Nifti1Image(extractedRegion,self.mniInputImage.get_affine(),self.mniInputImageInputHeader)
		croppedROI = extractedRegionNifTI
		delx,dely,delz = (0,0,0)
		pad = ((0,0),(0,0),(0,0))
		try:
			croppedROI = image.crop_img(img=extractedRegionNifTI,rtol=0.1,copy=True)
			delx,dely,delz = self.roiDimensions - np.array(croppedROI.get_data().shape)
			pad = ((int(np.floor(delx/2.)),int(np.ceil(delx/2.))),(int(np.floor(dely/2.)),int(np.ceil(dely/2.))),(int(np.floor(delz/2.)),int(np.ceil(delz/2.))))
		except Exception, e:
			print str(label)
			pass
		# delx,dely,delz = self.roiDimensions - np.array(croppedROI.get_data().shape)
		# pad = ((int(np.floor(delx/2.)),int(np.ceil(delx/2.))),(int(np.floor(dely/2.)),int(np.ceil(dely/2.))),(int(np.floor(delz/2.)),int(np.ceil(delz/2.))))
		extractedRegion = croppedROI.get_data()
		extractedRegionPadded = np.pad(extractedRegion,pad,mode='constant')
		extractedRegionNifTI = nibabel.Nifti1Image(extractedRegionPadded,self.mniInputImage.get_affine(),self.mniInputImageInputHeader)

		print "Saving "+str(label)+" ROI ...",
		nibabel.save(extractedRegionNifTI,outputDir+"/roi_"+str(label)+".nii")
		print  colored.green('\t[SAVED]')

	def extractROIs(self,outputDir):
		masks = [f for f in os.listdir("mask/") if f.split('.')[1] == 'npy']
		for mask in masks:
			file_ = open("mask/"+mask, 'rb')
			maskRegion = np.load(file_)
			self._extractROI(maskRegion,mask.split('.')[0],outputDir)

	def _findLargestDimensions(self,Dir,ROIs):
		x,y,z = 0,0,0
		for ROI in ROIs:
			ROInii = nibabel.load(Dir+"/"+ROI)
			if ROInii.get_data().sum() == 0:
				# print 'roi/smwrc1S10/'+ROI
				continue
			croppedROI = image.crop_img(img=ROInii,rtol=0.1,copy=True)
			__x,__y,__z = croppedROI.get_data().shape
			x = __x if __x > x else x
			y = __y if __y > y else y
			z = __z if __z > z else z
		return x,y,z

	def findLargestDimensions(self):
		print "calculating max ROI dimensions..."
		Dirs = [x[0] for x in os.walk("roi/")][1:]
		x,y,z = 0,0,0
		for Dir in Dirs:
			ROIs = [f for f in os.listdir(Dir) if f.split('.')[1] == 'nii']
			_x,_y,_z = self._findLargestDimensions(Dir,ROIs)
			x = _x if _x > x else x
			y = _y if _y > y else y
			z = _z if _z > z else z
		print "Max dimensions are: ",
		print x,y,z

	def calculateRegionalVolume(self):
		targets = []
		inputs = []
		with open('PatientsNoBiasHCAD.csv', 'rbU') as csvfile:
			reader = csv.reader(csvfile, delimiter=',', quotechar='\n')
			for row in reader:
					targets.append(row[1])
					inputs.append(row[0])
		patients = zip(inputs,targets)

		print "Finding difference in ROI dimensions..."
		Dirs = [x for x in os.listdir("/Volumes/HLV BUFFALO/ROIData20/")]
		ROIs = [f.split('.')[0] for f in os.listdir("mask20/") if f.split('.')[1] == 'npy']
		csvfile = open('region_vol_HCAD.csv','wb')
		csvWrite = csv.writer(csvfile)
		
		labelRow = []
		# appending labels
		for ROI in ROIs:
			labelRow.append(ROI)
		labelRow.append('target')
		csvWrite.writerow(labelRow)
		print "labels written.",labelRow

		# writing data
		for inp,tar in patients:
			row = []
			for ROI in ROIs:
				print "writing ",ROI
				roiFile = nibabel.load("/Volumes/HLV BUFFALO/ROIData20/smwrc1S"+inp+"/roi_"+ROI+".nii")
				roiv = np.sum(roiFile.get_data())
				row.append(roiv)
			row.append(tar)
			csvWrite.writerow(row)
			
		csvfile.close()
		

	def findMeanVolumeDifference(self):
		meanRegionVolumesAD = []
		meanRegionVolumesHC = []
		targetArrayHC = []
		targetArrayAD = []
		with open('PatientsNoBiasHCAD.csv', 'rbU') as csvfile:
			reader = csv.reader(csvfile, delimiter=',', quotechar='\n')
			for row in reader:
				if int(row[1]) == 0:
					targetArrayHC.append(row[0])
				else:
					targetArrayAD.append(row[0])
		print "Finding difference in ROI dimensions..."
		DirsHC = [x for x in os.listdir("/Volumes/HLV BUFFALO/ROIData20/") if x.split("/")[len(x.split("/")) - 1].split("smwrc1S")[1] in targetArrayHC]
		DirsAD = [x for x in os.listdir("/Volumes/HLV BUFFALO/ROIData20/") if x.split("/")[len(x.split("/")) - 1].split("smwrc1S")[1] in targetArrayAD]
		ROIs = [f.split('.')[0] for f in os.listdir("mask/") if f.split('.')[1] == 'npy']
		figNum = 0
		
		# for csv 
		csvfile = open('meanVolumeSubregion.csv','wb')
		csvWrite = csv.writer(csvfile)
		csvWrite.writerow(('region','HC','AD'))
		for ROI in ROIs:
			row = []
			row.append(ROI)
			print ROI,'...',
			roiVolume = []
			for Dir in DirsHC:
				roiFile = nibabel.load("/Volumes/HLV BUFFALO/ROIData20/"+Dir+"/roi_"+ROI+".nii")
				roiv = np.sum(roiFile.get_data())
				roiVolume.append(roiv)
			meanRegionVolumesHC.append(np.mean(np.array(roiVolume))/1000.)
			row.append(np.mean(np.array(roiVolume)))
			roiVolume = []
			for Dir in DirsAD:
				roiFile = nibabel.load("/Volumes/HLV BUFFALO/ROIData20/"+Dir+"/roi_"+ROI+".nii")
				roiv = np.sum(roiFile.get_data())
				roiVolume.append(roiv)
			print "done"
			meanRegionVolumesAD.append(np.mean(np.array(roiVolume))/1000.)
			row.append(np.mean(np.array(roiVolume)))
			csvWrite.writerow(row)
		csvfile.close()
		plt.plot(meanRegionVolumesHC,'g-o',label='HC')
		plt.plot(meanRegionVolumesAD,'r-^',label='AD')
		plt.ylabel('Mean brain subregion volume')
		plt.xlabel('subregion ID')
		plt.legend()
		plt.savefig("meanVolDiff.png")

	def findVolumeDifference(self):
		targetArrayHC = []
		targetArrayAD = []
		with open('PatientsNoBiasHCAD.csv', 'rbU') as csvfile:
			reader = csv.reader(csvfile, delimiter=',', quotechar='\n')
			for row in reader:
				if int(row[1]) == 0:
					targetArrayHC.append(row[0])
				else:
					targetArrayAD.append(row[0])
		print "Finding difference in ROI dimensions..."
		DirsHC = [x for x in os.listdir("/Volumes/HLV BUFFALO/ROIData20/") if x.split("/")[len(x.split("/")) - 1].split("smwrc1S")[1] in targetArrayHC]
		DirsAD = [x for x in os.listdir("/Volumes/HLV BUFFALO/ROIData20/") if x.split("/")[len(x.split("/")) - 1].split("smwrc1S")[1] in targetArrayAD]
		ROIs = [f.split('.')[0] for f in os.listdir("mask/") if f.split('.')[1] == 'npy']
		figNum = 0
		for ROI in ROIs:
			figNum += 1
			print ROI,'...',
			roiVolume = []
			for Dir in DirsHC:
				roiFile = nibabel.load("/Volumes/HLV BUFFALO/ROIData20/"+Dir+"/roi_"+ROI+".nii")
				roiv = np.sum(roiFile.get_data())
				# print roiv
				# print roiv,Dir
				roiVolume.append(roiv)
			# print "-"*30
			plt.plot(roiVolume,'g-',label='HC')
			roiVolume = []
			for Dir in DirsAD:
				roiFile = nibabel.load("/Volumes/HLV BUFFALO/ROIData20/"+Dir+"/roi_"+ROI+".nii")
				roiv = np.sum(roiFile.get_data())
				# print roiv
				roiVolume.append(roiv)
			print "done"
			
			plt.plot(roiVolume,'r-',label='AD')
			plt.ylabel(ROI+' volume')
			plt.xlabel('Patient ID')
			plt.legend()
			plt.savefig('regionVolumes/'+ROI+"VolDiff.png")
			plt.cla()
			# plt.show()

	def findVolume(self):
		targetArrayHC = []
		targetArrayAD = []

		with open('PatientsNoBiasHCAD.csv', 'rbU') as csvfile:
			reader = csv.reader(csvfile, delimiter=',', quotechar='\n')
			for row in reader:
				if int(row[1]) == 0:
					targetArrayHC.append(row[0])
				else:
					targetArrayAD.append(row[0])
		print "Finding brain volumes..."
		roiNum = 1
		Brains = [f for f in os.listdir("/Users/abhinit/mri_work/MRI_Scans_paro/GM/") if f.split('.')[1] == 'nii']
		roiVolumeHC = []
		roiVolumeAD = []
		for Brain in Brains:
			roiFile = nibabel.load("/Users/abhinit/mri_work/MRI_Scans_paro/GM/"+Brain)
			roiv = np.sum(roiFile.get_data())			
			if Brain.split('.')[0].split('smwrc1S')[1] in targetArrayAD:
				roiVolumeAD.append(roiv)
				print roiv,'AD'
			elif Brain.split('.')[0].split('smwrc1S')[1] in targetArrayHC:
				roiVolumeHC.append(roiv)
				print roiv,'HC'
		print "done"
		print len(roiVolumeHC),len(roiVolumeAD)

		csvfileForWriting = open('meanVolumeSubregion.csv','wb')
		csvWrite = csv.writer(csvfileForWriting)
		vols = []
		vols.append(roiVolumeHC)
		vols.append(roiVolumeAD)
		print vols
		labels = ['HC','AD']

		plt.boxplot(vols)

		plt.ylabel('Cubic pixels')
		# plt.xlabel('Patient ID')
		# plt.plot(roiVolumeHC,'g-',label='HC volume')
		# plt.plot(roiVolumeAD,'r-',label='AD volume')
		# plt.legend()
		plt.show()

	def plotBoxplot(self):
		boxData = []
		with open('meanVolumeSubregionBoxPlot.csv', 'rbU') as csvfile:
			reader = csv.reader(csvfile, delimiter=',', quotechar='\n')
			for row in reader:
				newRow = []
				newRow.append(row[0])
				newRow.append(row[1])
				newRow.append(row[2])
				boxData.append()
				print row

if __name__ == '__main__':

	f = Figlet(font='slant')
	print colored.green(f.renderText('Bhaksala!'))
	print colored.red("I am not programmed to do any better! :)\n")

	try:
		template = sys.argv[1]
		labels = sys.argv[2]
		preprocessor = Preprocessor(template,labels)
	except Exception, e:
		logging.warning("Houston! We got no templates or labels!")
		raise e

	# 1. For creating masks...
	# preprocessor.createMasks()

	# 2. For extracting ROIs...
	# try:
	# 	fileName = sys.argv[3]
	# except Exception, e:
	# 	logging.warning("Houston! We got no filename!")
	# 	raise e
	# preprocessor.attachFile(fileName)
	# preprocessor.extractROIs('roi')

	# 3. For calculating maximum dimensions...
	# preprocessor.findLargestDimensions()

	# 4. To find possibilities...
	# preprocessor.calculateRegionalVolume()
	# preprocessor.findVolume()

	preprocessor.findVolume()

	#fin!

