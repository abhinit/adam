# ADAM
## Alzheimer's Disease Association Mining
---
This project aims at preprocessing brain MRI images, extracting specific sub 
regions of the brain using template masks and then using a deep neural network
to carry out feature selection and association learning between different
sub regions.
