import sys, os, time, random, csv

import theano
import theano.tensor as T
from theano import  function, printing, pp
import numpy as np

import nibabel

import matplotlib.pyplot as plt
import cPickle as pi

from pairEstimatorIndCV import PairEstimator

def adaboost():
	targetFile = '../PatientsNoBiasHCAD.csv'
	classifierDirectory = 'models/'
	dataDir = '/Volumes/HLV BUFFALO/ROIData20/'
	targetArray = []

	with open(targetFile, 'rbU') as csvfile:
		reader = csv.reader(csvfile, delimiter=',', quotechar='\n')
		for row in reader:
			targetArray.append(row)
	random.shuffle(targetArray)

	trainingSet = targetArray
	testSet = targetArray

	classifiers = [classifierDirectory+f for f in os.listdir(classifierDirectory) if f.split('.')[1] == 'pkl']

	# initial weights for each sample
	W = 1.0/len(targetArray) * np.ones(len(targetArray))

	# weighted error or epsilon
	alphaList = []
	chosenClassifierList = []
	nEpoch = 100

	for epoch in range(nEpoch):

		print "epoch {}/{}".format(epoch,nEpoch)

		allErrorList = []
		weightedErrorList = []
		for classifier in classifiers:
			print classifier

			region1 = classifier.split('/')[1].split('.')[0].split('+')[0]
			region2 = classifier.split('/')[1].split('.')[0].split('+')[1]

			errorList = []
			for idx,target in trainingSet:
				print idx,target
				predictor = PairEstimator(mode='classify',model=classifier)
				i1 = nibabel.load(dataDir+'smwrc1S{}/roi_{}.nii'.format(idx,region1)).get_data()
				i2 = nibabel.load(dataDir+'smwrc1S{}/roi_{}.nii'.format(idx,region2)).get_data()
				prediction = predictor.run(i1,i2)
				errorList.append(int(np.not_equal(int(target),prediction)))
			errorList = np.array(errorList)
			allErrorList.append(errorList)
			weightedError = np.dot(W,np.transpose(errorList))
			weightedErrorList.append(weightedError)

		print weightedError

		epsilonIndex = weightedErrorList.index(min(weightedErrorList))
		print "Best classifier error:",weightedErrorList[epsilonIndex],"for classifier no.",epsilonIndex

		if weightedErrorList[epsilonIndex] > 0.5:
			# no more good classifiers left
			print "Stopping!!"
			break

		alpha = 0.5 * np.log((1 - weightedErrorList[epsilonIndex])/weightedErrorList[epsilonIndex])

		# the classifier and it's weightage
		alphaList.append(alpha)
		chosenClassifierList.append(epsilonIndex)

		# data weight update
		# if error then -1 else 1
		chosenError = allErrorList[epsilonIndex]
		idx = 0
		for e in chosenError:
			if e == 1:
				chosenError[idx] = -1
			else:
				chosenError[idx] = 1
			idx += 1

		W = W * np.exp(-(alpha*chosenError))
		Wsum = np.sum(W)
		W = W/Wsum

	print '-'*30
	print "Finally we have all the classifiers and their alphas ..."
	print alphaList, chosenClassifierList

	print "let's clean it up!"
	_alphaList,_chosenClassifierList = [],[]
	for a,c in zip(alphaList,chosenClassifierList):
		if c not in _chosenClassifierList:
			_alphaList.append(a)
			_chosenClassifierList.append(c)
		else:
			cIdx = _chosenClassifierList.index(c)
			_alphaList[cIdx] += a
	chosenClassifierList,alphaList = _chosenClassifierList,_alphaList

	print alphaList, chosenClassifierList	
	print '-'*30

	print "let's test the performance..."
	testErrorList = []
	for testidx,testtarget in testSet:
		prediction = None
		predictionList = []
		alphaError = 0.
		testError = None

		for classifier in classifiers:
			region1 = classifier.split('/')[1].split('.')[0].split('+')[0]
			region2 = classifier.split('/')[1].split('.')[0].split('+')[1]
			predictor = PairEstimator(mode='classify',model=classifier)
			i1 = nibabel.load(dataDir+'smwrc1S{}/roi_{}.nii'.format(testidx,region1)).get_data()
			i2 = nibabel.load(dataDir+'smwrc1S{}/roi_{}.nii'.format(testidx,region2)).get_data()
			prediction = predictor.run(i1,i2)
			predictionList.append(prediction)

		for classifierIndex,alpha in zip(chosenClassifierList,alphaList):
			alphaError += alpha * predictionList[classifierIndex]

		if alphaError >= 0:
			prediction = 1
		else:
			prediction = 0

		testError = int(np.not_equal(prediction,int(testtarget)))
		testErrorList.append(testError)

	testError = np.sum(np.array(testErrorList))*100./len(testErrorList)
	# print testErrorList,alphaList

	# saving the alphaList and chosenClassifierList
	toBeSaved = []
	toBeSaved.append(testErrorList)
	toBeSaved.append(testError)
	toBeSaved.append(alphaList),
	toBeSaved.append(chosenClassifierList)
	print toBeSaved
	with open('adaboost.pkl','wb') as adaboostFile:
		pi.dump(toBeSaved,adaboostFile)
	adaboostFile.close()


if __name__ == '__main__':
	os.system('python pairEstimatorIndCV.py')
	adaboost()
