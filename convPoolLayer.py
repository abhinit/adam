import numpy as np
import theano
import theano.tensor as T
import theano.tensor.nnet.conv3d2d as conv3d2d
# from maxpool3d import maxpool3d as downsample3d
from maxpool3d2d import max_pool_3d as downsample3d

class ConvPoolLayer(object):
	W_bound,filter_shape,b_values,rng = None,None,None,None
	def __init__(self, rng, input, filter_shape, image_shape, poolsize,w=None,b=None):
		# assert that the number of filters and images are the same...
		assert image_shape[2] == filter_shape[2]
		self.input = input
		
		if w is None:
			fan_in = np.prod(filter_shape[2:])
			fan_out = (filter_shape[0] * np.prod(filter_shape[3:]) /
					   np.prod(poolsize))
			W_bound = np.sqrt(6. / (fan_in + fan_out))
			self.W_bound = W_bound
			self.rng = rng
			self.filter_shape = filter_shape
			self.w = theano.shared(
				np.asarray(
					rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
					dtype=theano.config.floatX
				),
				name='wConvPool',
				borrow=True
			)
			# theano.shared(np.asarray(rng.uniform(low=-0.80064076902544, high=0.80064076902544, size=(5,3,1,3,3)),dtype=theano.config.floatX),name='wConvPool',borrow=True)
		else:
			w_values = np.load(w)
			self.w = theano.shared(value=w_values,name='wConvPool',borrow=True)

		if b is None:
			b_values = np.zeros((filter_shape[0],), dtype=theano.config.floatX)
			self.b_values = b_values
			self.b = theano.shared(value=b_values, borrow=True,name='b')
		else:
			b_values = np.load(b)
			self.b = theano.shared(value=b_values, borrow=True,name='b')


		# convolve input feature maps with filters
		conv_out = conv3d2d.conv3d(
			signals=self.input,
			filters=self.w,
			filters_shape=filter_shape,
			signals_shape=image_shape
		)

		conv_out = conv_out.dimshuffle(0,2,4,3,1)
		self.pooled_out = downsample3d(
			input=conv_out,
			ds=poolsize,
			ignore_border=True
		)
		self.pooled_out = self.pooled_out.dimshuffle(0,4,1,3,2)


		# add the bias term. Since the bias is a vector (1D array), we first
		# reshape it to a tensor of shape (1, n_filters, 1, 1). Each bias will
		# thus be broadcasted across mini-batches and feature map
		# width & height
		# relu = lambda x: x * (x > 1e-6)
		x = self.pooled_out + self.b.dimshuffle('x', 'x', 0, 'x', 'x')
		# self.output = relu(x)
		# self.output = T.switch(x<0, 0, x)
		self.output = T.tanh(x)
		self.params = [self.w, self.b]

		# print 'ConvPool layer created'


	def resetParams(self):
		b_values = np.zeros((self.filter_shape[0],), dtype=theano.config.floatX)
		self.w = theano.shared(
			np.asarray(
				self.rng.uniform(low=-self.W_bound, high=self.W_bound, size=self.filter_shape),
				dtype=theano.config.floatX
			),
			name='wConvPool',
			borrow=True
		)
		self.b = theano.shared(value=b_values, borrow=True,name='b')

	def setParams(self,w,b):
		self.w = theano.shared(
			value=w,
			name='wOutputLayer',
			borrow=True
		)

		self.b = theano.shared(
			value=b,
			name='b', 
			borrow=True
		)
