import theano
import numpy as np
import theano.tensor as T
import nibabel
import random

def loadData(data_xy, shuffle=True, borrow=True):
	_data_x, _data_y = data_xy
	_data_xy = zip(_data_x,_data_y)
	if shuffle is True:
		random.shuffle(_data_xy)
	
	data_x, data_y = [],[]

	for data_name, data_value in _data_xy:
		roiData = nibabel.load(data_name).get_data()
		data_x.append(roiData)
		data_y.append(data_value)

	shared_x = np.asarray(data_x,dtype=theano.config.floatX)
	shared_y = np.asarray(data_y,dtype=theano.config.floatX)
	
	return shared_x, shared_y.astype(np.int32)

	



