import theano
import theano.tensor as T
from theano.tensor.nnet import sigmoid
import numpy as np

class OutputLayer(object):
	n_in, n_out = 0,0
	def __init__(self, input, n_in, n_out,w=None,b=None):
		
		self.n_in, self.n_out = n_in,n_out
		self.input = input
		self.y = None
		
		if w is None:
			self.w = theano.shared(
				value=np.zeros(
					(n_in, n_out),
					dtype=theano.config.floatX
				),
				name='w',
				borrow=True
			)
		else:
			w_values = np.load(w)
			self.w = theano.shared(
				value=np.array(
					w_values,
					dtype=theano.config.floatX
					),
				name='wOutputLayer',
				borrow=True
			)

		# initialize the baises b as a vector of n_out 0s
		if b is None:
			self.b = theano.shared(
				value=np.zeros(
					(n_out,),
					dtype=theano.config.floatX
				),
				name='b',
				borrow=True
			)
		else:
			b_values = np.load(b)
			self.b = theano.shared(
				value=np.array(b_values,
					dtype=theano.config.floatX),
				name='b', 
				borrow=True
			)
		
		self.p_y_given_x = T.nnet.softmax(T.dot(self.input, self.w) + self.b)
		self.y_pred = T.argmax(self.p_y_given_x,axis=1)
		self.params = [self.w, self.b]

	def setParams(self,w,b):
		self.w = theano.shared(
			value=w,
			name='wOutputLayer',
			borrow=True
		)

		self.b = theano.shared(
			value=b,
			name='b', 
			borrow=True
		)

	def resetParams(self):

		self.w = theano.shared(
			value=np.zeros(
				(self.n_in, self.n_out),
				dtype=theano.config.floatX
			),
			name='w',
			borrow=True
		)
		self.b = theano.shared(
			value=np.zeros(
				(self.n_out,),
				dtype=theano.config.floatX
			),
			name='b',
			borrow=True
		)
		

	def negative_log_likelihood(self,y):
		self.y = y
		return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]),y])

	def errors(self, y):
		if y.ndim != self.y_pred.ndim:
			raise TypeError(
				'y should have the same shape as self.y_pred',
				('y', y.type, 'y_pred', self.y_pred.type)
			)
		if y.dtype.startswith('int'):
			return T.mean(T.neq(self.y_pred, y))
		else:
			raise NotImplementedError()
