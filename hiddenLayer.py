import theano
import theano.tensor as T
import numpy as np

class HiddenLayer(object):
	n_in,n_out,rng = None,None,None
	def __init__(self, rng, input, n_in, n_out, w=None, b=None, activation=T.tanh):
		self.input, self.rng = input, rng
		self.n_in, self.n_out = n_in, n_out
		if w is None:
			w_values = np.asarray(
				rng.uniform(
					low=-np.sqrt(6. / (n_in + n_out)),
					high=np.sqrt(6. / (n_in + n_out)),
					size=(n_in, n_out)
				),
				dtype=theano.config.floatX
			)
			if activation == theano.tensor.nnet.sigmoid:
				w_values *= 4
			self.w_values = w_values
			w = theano.shared(value=w_values, name='w', borrow=True)
		else:
			w_values = np.load(w)
			w = theano.shared(
				value=np.array(w_values,
					dtype=theano.config.floatX
					), 
				name='wHiddenLayer', 
				borrow=True
			)

		if b is None:
			b_values = np.zeros((n_out,), dtype=theano.config.floatX)
			self.b_values = b_values
			b = theano.shared(value=b_values, name='b', borrow=True)
		else:
			b_values = np.load(b)
			b = theano.shared(
				value=np.array(b_values,
					dtype=theano.config.floatX),
				name='b', 
				borrow=True
			)

		self.w = w
		self.b = b
		lin_output = T.dot(input, self.w) + self.b
		self.output = (
			lin_output if activation is None
			else activation(lin_output)
		)

		self.params = [self.w, self.b]

	def setParams(self,w,b):
		self.w = theano.shared(
			value=w,
			name='wOutputLayer',
			borrow=True
		)

		self.b = theano.shared(
			value=b,
			name='b', 
			borrow=True
		)

	def resetParams(self):
		w_values = np.asarray(
			self.rng.uniform(
				low=-np.sqrt(6. / (self.n_in + self.n_out)),
				high=np.sqrt(6. / (self.n_in + self.n_out)),
				size=(self.n_in, self.n_out)
			),
			dtype=theano.config.floatX
		)
		b_values = np.zeros((self.n_out,), dtype=theano.config.floatX)

		self.w = theano.shared(value=w_values, name='w', borrow=True)
		self.b = theano.shared(value=b_values, borrow=True,name='b')


