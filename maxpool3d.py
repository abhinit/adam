import numpy as np
import theano
import theano.tensor as T
import nibabel

def maxpool3d(input,ds,image_shape,stride = [1,1,1]):
	# Converting to np arrays
	# input = np.array(input)
	pooled_out = T.TensorType('float32', (False,)*5)('pooled_out')
	pooled_inShuffle = input.dimshuffle(0,2,4,3,1)
	inputShape=(image_shape[4],image_shape[3],image_shape[1])
	nMaps = image_shape[2]
	maxPoolOutputShape = np.divide(inputShape,ds)
	maxPoolOutput = T.ftensor3('maxPoolOutput')
	pooled_in = T.ftensor3('maxPoolOutput')

	for n in range(nMaps):
		pooled_in = pooled_inShuffle[0,n,:,:,:]		

		# maxPoolOutput = np.empty((maxPoolOutputShape))
		ds = np.array(ds)
		Xdim, Ydim, Zdim = maxPoolOutputShape

		# calculate necessary padding
		xpad = int(np.floor((inputShape[0]%ds[0])/2))
		ypad = int(np.floor((inputShape[1]%ds[1])/2))
		zpad = int(np.floor((inputShape[2]%ds[2])/2))

		# temp pad to use once during the first block
		gap = [xpad,ypad,zpad]
		_x,_y,_z = gap
		# massive loop begins!
		for xi in range(1,Xdim+1):
			x = int(xi*ds[0]+gap[0])
			for yi in range(1,Ydim+1):
				y = int(yi*ds[1]+gap[1])
				for zi in range(1,Zdim+1):
					z = int(zi*ds[2]+gap[2])
					block = pooled_in[_x:x,_y:y,_z:z]
					blockMax = T.max(block)
					T.set_subtensor(maxPoolOutput[xi-1,yi-1,zi-1],blockMax)
					_z = int(zi*ds[2]+gap[2])
				_z = zpad
				_y = int(yi*ds[1]+gap[1])
			_y = ypad
			_x = int(xi*ds[0]+gap[0])
		_x = xpad

		T.set_subtensor(pooled_out[0,n,:,:,:],maxPoolOutput)

	pooled_out = pooled_out.dimshuffle(0,4,2,3,1)
	return pooled_out


