import sys, os, time, random

import theano
import theano.tensor as T
from theano import  function, printing, pp
import numpy as np
import itertools

from convPoolLayer import ConvPoolLayer
from hiddenLayer import HiddenLayer
from fullLayer import OutputLayer
from dataLoader import sharedDataset

import cPickle as pi

from subprocess import call

class PairEstimator(object):
	dataset = []
	batchSize = 0
	mode = 'train'
	def __init__(self,inputDim,imageShapes,filterShapes,learningRate,nEpochs,mode='train',model=None,dbDirs=None):
		if dbDirs is None:
			print "No datasets found for the pair estimator!"
			sys.exit(0)
		else:
			# We need a pair!...
			assert len(dbDirs) == 2

		for dbName in dbDirs:
			if dbName.split('.')[len(dbName.split('.')) - 1] == 'npy':
				with open(dbName,'rb') as dbFile:
					self.dataset.append(np.load(dbFile))

		self.batchSize = imageShapes[0][0]
		self.shuffleData()

		# Get stuff for the online training
		# Get the input dimensions, filter shapes, image shapes at every layer, learning
		# rate and number of epochs
		self.inputDim = inputDim
		self.featureFilterShapes = filterShapes
		self.inputImageShapes = imageShapes
		self.learningRate = learningRate
		self.nEpochs = nEpochs

		# Lets process the model
		if model is not None:
			self.cp11w = model[0]
			self.cp11b = model[1]
			self.cp12w = model[2]
			self.cp12b = model[3]
			self.cp21w = model[4]
			self.cp21b = model[5]
			self.cp22w = model[6]
			self.cp22b = model[7]
			self.olw = model[8]
			self.olb = model[9]
		else:
			self.cp11w = None
			self.cp11b = None
			self.cp12w = None
			self.cp12b = None
			self.cp21w = None
			self.cp21b = None
			self.cp22w = None
			self.cp22b = None
			self.olw = None
			self.olb = None


		# Good to go!

	def shuffleData(self):
		self.trainSetX1, self.trainSetY1 = sharedDataset(self.dataset[0][0])
		self.validSetX1, self.validSetY1 = sharedDataset(self.dataset[0][1],shuffle=True)
		self.testSetX1, self.testSetY1 = sharedDataset(self.dataset[0][2],shuffle=False)

		self.trainSetX2, self.trainSetY2 = sharedDataset(self.dataset[1][0])
		self.validSetX2, self.validSetY2 = sharedDataset(self.dataset[1][1],shuffle=True)
		self.testSetX2, self.testSetY2 = sharedDataset(self.dataset[1][2],shuffle=False)

		# compute number of minibatches for training, validation and testing
		self.nTrainBatches = self.trainSetX1.get_value(borrow=True).shape[0]
		self.nValidBatches = self.validSetX1.get_value(borrow=True).shape[0]
		self.nTestBatches = self.testSetX1.get_value(borrow=True).shape[0]

		self.nTrainBatches /= self.batchSize
		self.nValidBatches /= self.batchSize
		self.nTestBatches /= self.batchSize

		# print "shuffled data ..."

	def run(self):
		idx = T.lscalar()
		# setting up inputs of the pipeline...
		input1 = T.ftensor4('input')
		input2 = T.ftensor4('input')

		# Target is the same for both!
		target = T.ivector('target')
		rng = np.random.RandomState(121212)

		# Creating the ConvPool layers...
		cp11Input = input1.reshape((self.batchSize,self.inputDim[2],1,self.inputDim[1],self.inputDim[0]))
		cp21Input = input2.reshape((self.batchSize,self.inputDim[2],1,self.inputDim[1],self.inputDim[0]))

		# First CNN
		self.cp11 = ConvPoolLayer(
			rng, 
			input=cp11Input,
			filter_shape=self.featureFilterShapes[0],
			image_shape=self.inputImageShapes[0],
			poolsize=(5, 5, 5),
			w=self.cp11w,
			b=self.cp11b
		)

		self.cp12 = ConvPoolLayer(
			rng, 
			input=self.cp11.output,
			filter_shape=self.featureFilterShapes[1],
			image_shape=self.inputImageShapes[1],
			poolsize=(4, 4, 4),
			w=self.cp12w,
			b=self.cp12b
		)

		# Second CNN
		self.cp21 = ConvPoolLayer(
			rng, 
			input=cp21Input,
			filter_shape=self.featureFilterShapes[0],
			image_shape=self.inputImageShapes[0],
			poolsize=(5, 5, 5),
			w=self.cp21w,
			b=self.cp21b
		)

		self.cp22 = ConvPoolLayer(
			rng, 
			input=self.cp21.output,
			filter_shape=self.featureFilterShapes[1],
			image_shape=self.inputImageShapes[1],
			poolsize=(4, 4, 4),
			w=self.cp22w,
			b=self.cp22b
		)

		hl1Input = self.cp12.output.flatten(2)
		hl2Input = self.cp22.output.flatten(2)

		# Creating the hidden layer...
		hlPairInput = T.concatenate([hl1Input,hl2Input],axis=1)

		#Creating the outputlayer
		n_in = 48
		n_out = 2
		self.ol = OutputLayer(
			input=hlPairInput,
			n_in=n_in,
			n_out=n_out,
			w=self.olw,
			b=self.olb
		)

		cost = self.ol.negative_log_likelihood(target)
		errors = self.ol.errors(target)
		olProbability = self.ol.p_y_given_x
		olPrediction = self.ol.y_pred

		# create a list of all model parameters 
		# to be fit by gradient descent and
		# calculate the gradient
		params = self.ol.params + self.cp22.params + self.cp12.params + self.cp21.params + self.cp11.params

		grads = T.grad(cost, params)


		# update the params
		updates = [
			(param_i, param_i - self.learningRate * grad_i)
			for param_i, grad_i in zip(params, grads)
		]

		#Finally defining a theano function
		#to do all the heavy lifting!
		trainPairModel = theano.function(
			[idx],
			[cost,
			errors,
			olProbability,
			olPrediction,
			self.ol.y
			],
			updates=updates,
			givens={
				input1: self.trainSetX1[idx * self.batchSize: (idx + 1) * self.batchSize],
				input2: self.trainSetX2[idx * self.batchSize: (idx + 1) * self.batchSize],
				target: self.trainSetY1[idx * self.batchSize: (idx + 1) * self.batchSize]
			}
		)

		validatePairModel = theano.function(
			[idx],
			self.ol.errors(target),
			givens={
				input1: self.validSetX1[idx * self.batchSize: (idx + 1) * self.batchSize],
				input2: self.validSetX2[idx * self.batchSize: (idx + 1) * self.batchSize],
				target: self.validSetY1[idx * self.batchSize: (idx + 1) * self.batchSize]
			}
		)

		testPairModel = theano.function(
			[idx],
			self.ol.errors(target),
			givens={
				input1: self.testSetX1[idx * self.batchSize: (idx + 1) * self.batchSize],
				input2: self.testSetX2[idx * self.batchSize: (idx + 1) * self.batchSize],
				target: self.testSetY1[idx * self.batchSize: (idx + 1) * self.batchSize]
			}
		)


		# If the mode is training
		if self.mode == "train":

			model = []

			# print 'And God said,\"Let the training begin!\"'

			# early-stopping parameters
			patience = 5  # look as this many examples regardless
			patienceIncrease = 5  # wait this much longer when a new best is found
			improvementThreshold = 0.995  # a relative improvement of this much is
										   # considered significant
			validationFrequency = min(self.nTrainBatches, patience / 2)
										  # go through this many
										  # minibatche before checking the network
										  # on the validation set; in this case we
										  # check every epoch
			maxPatienceDifference = 5  # This is the amount of samples we look
										  # for a new best after we have got the
										  # new best
										  # 80 more epochs!

			bestValidationLoss = np.inf
			bestIter = 0
			testScore = 0.
			startTime = time.clock()

			epoch = 0
			doneLooping = False

			#outputState variables
			self.errorTrainingList = []
			self.errorValidationList = []
			self.errorTestList = []
			self.predList = []
			self.targetList = []
			currentIterValidationLoss = 0

			while (epoch < self.nEpochs) and (not doneLooping):
				epoch = epoch + 1
				# print 'epoch {}/{}'.format(epoch,self.nEpochs)

				#emptying the outputState lists
				self.predList[:] = []
				self.targetList[:] = []
				costWhileTraining, errorWhileTraining, probability, prediction, y = None,None,None,None,None

				for index in xrange(self.nTrainBatches):
					self.shuffleData()
					iter = (epoch - 1) * self.nTrainBatches + index

					# if iter % 100 == 0:
					# 	print 'training @ iter = ', iter
					trainingErrorList = []
					costWhileTraining, errorWhileTraining, probability, prediction,y = trainPairModel(index)
					trainingErrorList.append(errorWhileTraining)


					# print 'training @ iter = ', iter
					# print 'epoch {}/{}'.format(epoch,self.nEpochs)
					# print 'training {}/{}'.format(index,self.nTrainBatches)
					# print 'prediction: {}'.format(prediction)
					# print 'yyy	   : {}'.format(y)
					# print patience, iter*patienceIncrease - maxPatienceDifference
					# print '---------------------'

					if (iter + 1) % validationFrequency == 0:
						validationLosses = [validatePairModel(i) for i in xrange(self.nValidBatches)]
						currentIterValidationLoss = np.mean(validationLosses)
						trainingErrorList = np.array(trainingErrorList)
						print "training error is: {}".format(np.mean(errorWhileTraining) * 100.)
						print('epoch %i, minibatch %i/%i, validation error %f %%' %
							  (epoch, index + 1, self.nTrainBatches,
							   currentIterValidationLoss * 100.)),
						print patience, iter*patienceIncrease

						if currentIterValidationLoss < bestValidationLoss * improvementThreshold:
							patience = max(patience, iter * patienceIncrease)
							bestValidationLoss = currentIterValidationLoss
							bestIter = iter
							#testing on the test set
							testLosses = [testPairModel(i) for i in xrange(self.nTestBatches)]
							testScore = np.mean(testLosses)

							# First clear the previous one!
							model = []
							
							# This is where we save the model
							model.append(self.cp11.params[0].get_value())
							model.append(self.cp11.params[1].get_value())
							model.append(self.cp12.params[0].get_value())
							model.append(self.cp12.params[1].get_value())
							model.append(self.cp21.params[0].get_value())
							model.append(self.cp21.params[1].get_value())
							model.append(self.cp22.params[0].get_value())
							model.append(self.cp22.params[1].get_value())
							model.append(self.ol.params[0].get_value())
							model.append(self.ol.params[1].get_value())

							print "Model saved!",

							print(('epoch %i, minibatch %i/%i, test error of '
								   'best model %f %%') %
								  (epoch, index + 1, self.nTrainBatches,
								   testScore * 100.))
							
					if patience == iter*patienceIncrease - maxPatienceDifference:
						# print patience, iter*patienceIncrease, "we are done waiting!"
						doneLooping = True
						break
				
					self.errorTestList.append(testScore)
					self.errorTrainingList.append(errorWhileTraining)
					self.errorValidationList.append(currentIterValidationLoss)

			endTime = time.clock()
			print('Optimization complete.')
			print('Best validation score of %f %% obtained at iteration %i, '
				  'with test performance %f %%' %
				  (bestValidationLoss * 100., bestIter + 1, testScore * 100.))
			print >> sys.stderr, ('The code for file ' +
								  os.path.split(__file__)[1] +
								  ' ran for %.2fm' % ((endTime - startTime) / 60.))

			model.append(bestValidationLoss * 100.)
			model.append(testScore * 100.)
			model.append(bestIter + 1)

			return model

			# plt.plot(xrange(len(self.errorTrainingList)), np.array(self.errorTrainingList), 'b-', xrange(len(self.errorTrainingList)), np.array(self.errorValidationList), 'g-', xrange(len(self.errorTestList)), np.array(self.errorTestList), 'r-')
			# plt.show()


if __name__ == '__main__':
	batchSize = 7
	labelFile = sys.argv[1]
	labelArray = [line.split()[0] for line in open(labelFile).readlines()]
	labelCombinations = [subset for subset in itertools.combinations(labelArray, 2)]
	for subset in labelCombinations[5:6]:
		print "\ntraining with {} and {}\n".format(subset[0],subset[1])
		pairEstimator = PairEstimator(dbDirs=['../database/database_roi_'+subset[0]+'.npy','../database/database_roi_'+subset[1]+'.npy'],
									inputDim=[33,57,49],
									filterShapes=[(5,3,1,3,3),(12,3,5,3,3)], 
									imageShapes=[(batchSize,49,1,57,33),(batchSize,9,5,11,6)],
									learningRate = 0.085,
									nEpochs=1000)
		model = pairEstimator.run()
		# print "validation% | test% | bestIter"
		# print model
		with open("model_{}_{}.pkl".format(subset[0],subset[1]),'wb') as modelFile:
			pi.dump(model,modelFile)

	
