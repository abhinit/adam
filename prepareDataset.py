#!/usr/bin python2.7
"""
Author: Abhinit Ambastha
Email: abhinit@comp.nus.edu.sg

This function prepares datasets from the extracted extractROIs
For e.g. (X_k,Y_N) where X = {x0,x1,x2,...,xN} is the ROI_k from
all the patients and Y_N is the Alzheimer's status of the patients
"""
import sys, time, os, csv, random
from os import path
from clint.textui import colored
import numpy as np
import cPickle as pi

if __name__ == '__main__':
	try:
		dataDir = sys.argv[1]
		targetFile = sys.argv[2]
		maskDir = sys.argv[3]
		outputDir = sys.argv[4]
	except Exception, e:
		logging.warning("Houston! We got a problem!")
		print e

	# Correcting the trailing slash
	if dataDir[len(dataDir)-1] != '/':
		dataDir = dataDir + '/'
	if outputDir[len(outputDir)-1] != '/':
		outputDir = outputDir + '/'
	if maskDir[len(maskDir)-1] != '/':
		maskDir = maskDir + '/'		

	targetArray = []

	# Let's get all the targets in targetArray
	with open(targetFile, 'rbU') as csvfile:
		reader = csv.reader(csvfile, delimiter=',', quotechar='\n')
		for row in reader:
			targetArray.append(row)
	random.shuffle(targetArray)

	roiNames = ["roi_"+f.split('.')[0] for f in os.listdir(maskDir) if f.split('.')[1] == 'npy']
	startTime = time.time()
	for roiName in roiNames:
		# Got it!

		# Create a database file
		# Save the filepath and target for every patient in a CSV file
		# [
		#  ...
		#  "roi/smwrc1S1/roi_Amygdala_L.nii":1,
		#  "roi/smwrc1S2/roi_Amygdala_L.nii":1,
		#  "roi/smwrc1S3/roi_Amygdala_L.nii":2,
		#  ...
		# ]
		# Walk through all the directories and collect the ROI given from each

		# Dirs = [x[0] for x in os.walk(dataDir)][1:]

		# Final database object with training, test and validation databases
		# Dividing it in %
		# Training: 70%
		# Test: 20%
		# validation: 10%
		trainingPercentage = 0.7
		validationPercentage = 0.15
		testingPercentage = 0.15
		database = [],[],[]
		totalNumRecords = len(targetArray)
		trainingSetSize, testSetSize, validationSetSize = int(np.ceil(trainingPercentage*totalNumRecords)),\
								int(np.floor(testingPercentage*totalNumRecords)),\
								int(np.floor(validationPercentage*totalNumRecords))
		print trainingSetSize, testSetSize, validationSetSize
		inputFiles,inputTargets = [],[]

		# for Dir in Dirs:
		# 	patientID = Dir.split("/")[len(Dir.split("/")) - 1].split("smwrc1S")[1]
		# 	ROI = [f for f in os.listdir(Dir) if f == roiName+'.nii']
		# 	inputFiles.append(Dir+"/"+ROI[0])
		# 	inputTargets.append([t for t in targetArray if t[0] == patientID][0][1])

		for patientID,target in targetArray:
			inputFile = dataDir+"smwrc1S"+patientID+"/"+roiName+'.nii'
			print inputFile,target
			inputFiles.append(inputFile)
			inputTargets.append(target)			

		database[0].append(inputFiles[0:trainingSetSize])
		database[0].append(inputTargets[0:trainingSetSize])

		database[1].append(inputFiles[trainingSetSize:trainingSetSize+validationSetSize])
		database[1].append(inputTargets[trainingSetSize:trainingSetSize+validationSetSize])
		
		database[2].append(inputFiles[trainingSetSize+validationSetSize:trainingSetSize+testSetSize+validationSetSize])
		database[2].append(inputTargets[trainingSetSize+validationSetSize:trainingSetSize+testSetSize+validationSetSize])

		with open(outputDir+"/database_"+roiName+".npy","wb") as dbFile:
			np.save(dbFile,database)

		print colored.blue(u'\u2713'+" "+roiName+" Database processed and saved successfully!")

	print "\nTime taken: "+str(format((time.time() - startTime),'.2f'))+" seconds\n"
	# Time taken: 28.50 seconds
	# Time taken: 39.88 seconds


	#fin!
