#!/usr/bin python2.7
"""
Author: Abhinit Ambastha
Email: abhinit@comp.nus.edu.sg

This function extracts ROIs from all the 201 scans
"""
import sys, logging, time, os
from os import path
from clint.textui import colored
from Preprocess import Preprocessor

if __name__ == '__main__':
	try:
		template = sys.argv[1]
		labels = sys.argv[2]
		dataDir = sys.argv[3]
		outputDir = sys.argv[4]
		preprocessor = Preprocessor(template,labels)
	except Exception, e:
		logging.warning("Houston! We got a problem!")
		print e

	# Correcting the trailing slash
	if dataDir[len(dataDir)-1] != '/':
		dataDir = dataDir + '/'
	if outputDir[len(outputDir)-1] != '/':
		outputDir = outputDir + '/'

	# Get all the nii files in the data directory
	inputFiles = [f for f in os.listdir(dataDir) if f.split('.')[1] == 'nii']
	startTime = time.time()
	done = 0
	print "Beginning data processing. \n"
	for inputFile in inputFiles[1:]:
		print "processing "+inputFile+"\n"
		fileName = inputFile.split('.')[0]
		outputDirTemp = outputDir+fileName
		if not os.path.exists(outputDirTemp):
			os.makedirs(outputDirTemp)
		preprocessor.attachFile(dataDir+inputFile)
		preprocessor.extractROIs(outputDirTemp)
		done += 1

	print colored.blue('\n'+u'\u2713'+' '+ str(done)\
		+" files processed successfully\n")
	print "Time taken: "+str(format((time.time() - startTime),'.2f'))+" seconds"
	# Time taken: 91799.64 seconds


	#fin!