import sys, os, time, random
import itertools
import theano
import theano.tensor as T
from theano import  function, printing, pp
import numpy as np
from clint.textui import colored
import cPickle as pi

from pairEstimatorTrainer import PairEstimator

from multiprocessing import Pool

def mpTrain(mptrainer,comb):
	# mptrainer.ok(comb)
	mptrainer.train(comb)

class Trainer(object):
	learnedModels = {}
	labelArray = []
	labelCombinations = []

	def __init__(self,labelFile):
		# Let's get all the targets in labelArray
		self.labelArray = [line.split()[0] for line in open(labelFile).readlines()]

	def train(self,regions):
		print colored.blue("Training with {} and {} sub regions".format(regions[0],regions[1]))
		batchSize = 7
		nEpochs = 1000
		dbDir = '../database/'
		pairEstimator = PairEstimator(dbDirs=[dbDir+'database_roi_'+regions[0]+'.npy',dbDir+'database_roi_'+regions[1]+'.npy'],
								inputDim=[33,57,49],
								filterShapes=[(5,3,1,3,3),(12,3,5,3,3)], 
								imageShapes=[(batchSize,49,1,57,33),(batchSize,9,5,11,6)],
								learningRate = 0.05,
								nEpochs=nEpochs)
		self.learnedModels[regions[0]+'+'+regions[1]] = pairEstimator.run()
		# print colored.green(u'\u2713'+"Done with {}/{} classifer(s)".format(str(done),len(self.labelArray))),
		with open("model_{}_{}.pkl".format(regions[0],regions[1]),'wb') as modelFile:
			pi.dump(self.learnedModels,modelFile)
			print colored.green('[SAVED]')

	def ok(self,regions):
		with open("hello_{}_{}.pkl".format(regions[0],regions[1]),'wb') as modelFile:
			pi.dump(self.learnedModels,modelFile)
			print colored.green('[SAVED]')

if __name__ == '__main__':
	trainer = Trainer('../AAL_Labels.txt')
	trainer.labelCombinations = [subset for subset in itertools.combinations(trainer.labelArray, 2)]
	pool = Pool(processes=5)
	print "Starting training ..."
	for comb in trainer.labelCombinations[:5]:
		pool.apply_async(mpTrain,args=(trainer,comb))
	pool.close()
	pool.join()
