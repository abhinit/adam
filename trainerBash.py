import sys, os, time, random
import itertools
import theano
import theano.tensor as T
from theano import  function, printing, pp
import numpy as np
from clint.textui import colored
import cPickle as pi

from pairEstimatorTrainer import PairEstimator

class Trainer(object):
	learnedModel = None
	labelArray = []
	labelCombinations = []
	def __init__(self,labelFile):
		# Let's get all the targets in labelArray
		self.labelArray = [line.split()[0] for line in open(labelFile).readlines()]
		random.shuffle(self.labelArray)

	def train(self,regions):
		batchSize = 7
		nEpochs = 1000
		dbDir = '../database/'
		print regions
		pairEstimator = PairEstimator(dbDirs=[dbDir+'database_roi_'+regions[0]+'.npy',dbDir+'database_roi_'+regions[1]+'.npy'],
								inputDim=[33,57,49],
								filterShapes=[(5,3,1,3,3),(12,3,5,3,3)], 
								imageShapes=[(batchSize,49,1,57,33),(batchSize,9,5,11,6)],
								learningRate = 0.07,
								nEpochs=nEpochs)
		self.learnedModel = pairEstimator.run()

if __name__ == '__main__':
	trainer = Trainer('../AAL_Labels20.txt')
	trainer.labelCombinations = [subset for subset in itertools.combinations(trainer.labelArray, 2)]
	print "Starting training ..."
	for subset in trainer.labelCombinations:
		print colored.blue("Training with {} and {} sub regions".format(subset[0],subset[1]))
		trainer.train(subset)
		print colored.green(u'\u2713'+"Done"),
		with open("model_{}_{}.pkl".format(subset[0],subset[1]),'wb') as modelFile:
			pi.dump(trainer.learnedModel,modelFile)
			print colored.green('[SAVED]')